# Moleculer d1soft / Redis

Redis mixin for moleculer

## Install

```sh
npm add -E ioredis @moleculer-d1soft/redis
```

## Example

```ts
import { Service as MoleculerService, ServiceBroker, ServiceSettingSchema } from 'moleculer';
import { Service } from 'moleculer-decorators';
import { RedisMixin, RedisMixinOptions } from '@bukusaya/redis';

interface SomeServiceSettings extends ServiceSettingSchema, RedisMixinOptions {}

@Service({
    name: 'some-service',
    version: 1,
    settings: {
      redisHost: process.env.REDIS_HOST || 'localhost',
      redisPort: Number(process.env.REDIS_PORT) || 6379,
      redisPassword: process.env.REDIS_PASSWORD || '',
      redisDatabase: Number(process.env.REDIS_DATABASE) || 0,

      $secureSettings: ['redisPassword']
    },
    mixins: [new RedisMixin()]
})
export default class SomeService extends MoleculerService<SomeServiceSettings> {
  protected connection?: IORedis;

	public constructor(public broker: ServiceBroker) {
		super(broker);
	}
}

```