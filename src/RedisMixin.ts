import { Service, ServiceSchema } from 'moleculer';
import Redis, { Redis as IORedis } from 'ioredis';
import type { RedisMixinOptions } from './RedisMixinOptions';

export class RedisMixin implements Partial<ServiceSchema>, ThisType<Service> {
    private schema?: Partial<ServiceSchema<RedisMixinOptions>> & ThisType<Service>;
    public connection?: IORedis;

    public async created(): Promise<void> {
        this.connection = new Redis({
            host: this.schema?.settings?.redisHost,
            port: this.schema?.settings?.redisPort,
            db: this.schema?.settings?.redisDatabase,
            password: this.schema?.settings?.redisPassword,
        });
    
        return Promise.resolve();
    }
    
    public async stopped(): Promise<void> {
        if (this.connection) {
            this.connection.disconnect();
        }

        return Promise.resolve();
    }
}