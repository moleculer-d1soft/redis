export interface RedisMixinOptions {
  redisHost: string;
  redisPort: number;
  redisPassword: string;
  redisDatabase: number;
}
